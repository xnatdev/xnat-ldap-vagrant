# XNAT LDAP Server

This is a [Vagrant](https://www.vagrantup.com/) Environment for an [OpenLDAP](https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol) Server
for testing XNAT LDAP authentication development. I gratefully acknowledge that
this is based on the [LDAP Server in Vagrant](https://github.com/rgl/ldap-vagrant)
project by [Rui Lopes](http://ruilopes.com). I modified the project to:

* Add more detailed network configuration
* Use the [bento/ubuntu-16.04](https://app.vagrantup.com/bento/boxes/ubuntu-16.04) box
* Add [phpldapadmin](http://phpldapadmin.sourceforge.net/wiki/index.php/Main_Page)
* Configure SSL for Apache2
* Add extra user attributes to the base populate routine

## Details

This lets you easily test your application code against a real sandboxed server.

This uses the [slapd](http://www.openldap.org/software/man.cgi?query=slapd) daemon from [OpenLDAP](http://www.openldap.org/).

LDAP is described at [RFC 4510 (Technical Specification)](https://tools.ietf.org/html/rfc4510).

Also check the [OpenLDAP Server documentation at the Ubuntu Server Guide](https://help.ubuntu.com/lts/serverguide/openldap-server.html).

## Usage

Run `vagrant up` to configure the `ldap.xnat.org` LDAP server environment.

Configure your system `/etc/hosts` file with the `ldap.xnat.org` domain:

    10.1.1.22   ldap.xnat.org

The environment comes pre-configured with the following entries:

    uid=asmith,ou=users,dc=xnat,dc=org
    uid=bjones,ou=users,dc=xnat,dc=org
    uid=coates,ou=users,dc=xnat,dc=org
    uid=dfoobar,ou=users,dc=xnat,dc=org
    uid=eadams,ou=users,dc=xnat,dc=org
    uid=fmiller,ou=users,dc=xnat,dc=org
    uid=gfallen,ou=users,dc=xnat,dc=org
    uid=hjames,ou=users,dc=xnat,dc=org

To see how these were added take a look at the end of the [provision.sh](provision.sh) file.

To troubleshoot, watch the logs with `vagrant ssh` and `sudo journalctl --follow`.

## Future Development

Planned updates include:

* Ability to customize and configure the server
* Import schema functionality
* Multi-LDAP repository support
* Full cross-server TLS support (who knows, it might work now)
