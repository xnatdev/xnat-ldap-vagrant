#!/bin/bash
set -eux

export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get -y full-upgrade
apt-get -y autoremove
update-ca-certificates

config_organization_name=XNAT
config_fqdn=$(hostname --fqdn)
config_domain=$(hostname --domain)
config_domain_dc="dc=$(echo ${config_domain} | sed 's/\./,dc=/g')"
config_admin_dn="cn=admin,${config_domain_dc}"
config_admin_password=password

echo "127.0.0.1 ${config_fqdn}" >>/etc/hosts

apt-get install -y --no-install-recommends vim
cat >/etc/vim/vimrc.local <<'EOF'
syntax on
set background=dark
set esckeys
set ruler
set laststatus=2
set nobackup
autocmd BufNewFile,BufRead Vagrantfile set ft=ruby
EOF

# these anwsers were obtained (after installing slapd) with:
#
#   #sudo debconf-show slapd
#   sudo apt-get install debconf-utils
#   # this way you can see the comments:
#   sudo debconf-get-selections
#   # this way you can just see the values needed for debconf-set-selections:
#   sudo debconf-get-selections | grep -E '^slapd\s+' | sort
debconf-set-selections <<EOF
slapd slapd/password1 password ${config_admin_password}
slapd slapd/password2 password ${config_admin_password}
slapd slapd/domain string ${config_domain}
slapd shared/organization string ${config_organization_name}
EOF

apt-get install -y --no-install-recommends slapd ldap-utils phpldapadmin zip unzip python-setuptools
easy_install --upgrade pip
pip install --upgrade httpie

for FILE in /etc/phpldapadmin/config.php /etc/phpldapadmin/apache.conf /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/default-ssl.conf; do
    mv ${FILE} ${FILE}.bak
done

cp /vagrant/configs/000-default.conf /etc/apache2/sites-available/000-default.conf
cp /vagrant/configs/default-ssl.conf /etc/apache2/sites-available/default-ssl.conf
cp /vagrant/configs/ssl-params.conf /etc/apache2/conf-available/ssl-params.conf
cp /vagrant/configs/config.php /etc/phpldapadmin/config.php
cp /vagrant/configs/apache.conf /etc/phpldapadmin/apache.conf

chown -R root:www-data /etc/phpldapadmin
ln -s /etc/apache2/conf-available/ssl-params.conf /etc/apache2/conf-enabled/ssl-params.conf

echo Generating /etc/ssl/private/apache-selfsigned.key and /etc/ssl/certs/apache-selfsigned.crt. Suppressing output because it''s very noisy.
openssl req -x509 -nodes -days 730 -newkey rsa:2048 \
    -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt \
    -subj "/C=US/ST=MO/L=Saint Louis/O=Washington University School of Medicine/OU=Neuroinformatics Research Group/CN=ldap.xnat.org/emailAddress=jrherrick@wustl.edu" > /dev/null 2>&1

echo Generating /etc/ssl/certs/dhparam.pem. Suppressing output because it''s very noisy.
openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048 > /dev/null 2>&1

a2enmod ssl
a2enmod headers
a2ensite default-ssl
a2enconf ssl-params
apache2ctl configtest

service apache2 restart

cat /etc/sudoers.d/99_vagrant | sed 's/vagrant/xnat/' > /etc/sudoers.d/98_xnat

# create the users container.
# NB the `cn=admin,${config_domain_dc}` user was automatically created
#    when the slapd package was installed.
ldapadd -D ${config_admin_dn} -w ${config_admin_password} <<EOF
dn: ou=users,${config_domain_dc}
objectClass: organizationalUnit
ou: users
EOF

function upper() {
    echo "$(tr '[:lower:]' '[:upper:]' <<< ${1:0:1})${1:1}"
}

# add users.
function add_person {
    local user=(${2//;/ })
    local fname="$(upper ${user[0]})"
    local lname="$(upper ${user[1]})"
    local title="$(upper ${user[2]})"
    local uname="${user[0]:0:1}${user[1]}"
    local dname="${fname} ${lname}"
    ldapadd -D ${config_admin_dn} -w ${config_admin_password} <<EOF
dn: uid=${uname},ou=users,${config_domain_dc}
objectClass: inetOrgPerson
userPassword: $(slappasswd -s password)
uid: ${uname}
mail: ${uname}@${config_domain}
cn: ${fname} ${lname}
givenName: ${fname}
displayName: ${dname}
sn: ${lname}
title: ${title}
labeledURI: http://xnat.org/~${uname} ${dname}'s Home Page
jpegPhoto::$(base64 -w 66 /vagrant/avatars/avatar-${n}.jpg | sed 's,^, ,g')
EOF
}

users=("alice;smith;manager" "bob;jones;director" "carol;oates;writer" "dave;foobar;intern" "eve;adams;progenitor" "frank;miller;artist" "grace;fallen;scold" "henry;james;writer")
for n in "${!users[@]}"; do
    add_person ${n} "${users[${n}]}"
done

# show the configuration tree.
ldapsearch -Q -LLL -Y EXTERNAL -H ldapi:/// -b cn=config dn | grep -v '^$'

# show the data tree.
ldapsearch -x -LLL -b ${config_domain_dc} dn | grep -v '^$'

# search for users and print some of their attributes.
ldapsearch -x -LLL -b ${config_domain_dc} '(objectClass=person)' cn mail

